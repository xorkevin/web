# Code

## Java
- java8-openjdk

## Android
- ButterKnife
- Dagger2

## Python
- TensorFlow
- arrayfire-python
- Flake8 (style)
- hacking
- Radon
- pylint
- tqdm (progress bar)
- joblib (caching, saving to file)
- dill (save to file)
- click (cli)
- kivy (application development)
- PyInstaller
- numba
- PyOpenCL

## Rust
- Piston

# cli tools
- npm install -g cloc
- npm publish-please
- npm local-web-server
- thefuck

# deployment
- saltstack
- coreos
- openstack
- distelli

# database
- zerodb

# forum tools
- discourse

# static site
- roots
- hugo


# tools
- iron node
- atom
- openstf

# references
- tldr-pages
- zeal docs
- jupyter notebook
