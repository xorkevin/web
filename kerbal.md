# Kerbal 

## Modlist 
- KSP: 1.0.4 (Win32) - Unity: 4.6.4f1 - OS: Windows 8.1  (6.3.10240) 64bit
- Filter Extensions - 2.4.1
- Advanced Jet Engine - 2.4.1
- B9 Aerospace Procedural Parts - 0.40
- Community Resource Pack - 0.4.5
- Connected Living Space - 1.1.3.1
- Deadly Reentry - 7.2.2
- Ferram Aerospace Research - 0.15.5.1
- Firespitter - 7.1.4
- Kerbal Joint Reinforcement - 3.1.4
- KSP-AVC Plugin - 1.1.5
- ModularFlightIntegrator - 1.1.1
- Procedural Parts - 1.1.7
- RealChute - 1.3.2.3
- RealismOverhaul - 10.4.1
- RealSolarSystem - 10.2
- RemoteTech - 1.6.8
- Saturatable RW - 1.10.2
- SolverEngines - 1.11
- TestFlight - 1.3.1.1
- TestFlight - 1.3.1.1
- TextureReplacer - 2.4.10
- TAC Life Support - 0.11.1.20
- WASDEditorCameraContinued - 0.4.1

## Mods (old) 
- KSP: 1.0.4 (Win32) - Unity: 4.6.4f1 - OS: Windows 8.1  (6.3.10240) 64bit
- Filter Extensions - 2.4.1
- USI Tools - 0.5.2
- Advanced Jet Engine - 2.4.1
- B9 Aerospace Procedural Parts - 0.40
- Community Resource Pack - 0.4.5
- Connected Living Space - 1.1.3.1
- Deadly Reentry - 7.2.2
- DMagic Orbital Science - 1.0.8
- Ferram Aerospace Research - 0.15.5.1
- Firespitter - 7.1.4
- Interstellar Fuel Switch - 1.17
- RasterPropMonitor - 0.23.2
- Kerbal Attachment System - 0.5.4
- Kerbal Engineer Redux - 1.0.18
- Kerbal Joint Reinforcement - 3.1.4
- HyperEdit - 1.4.1
- kOS - 0.17.3
- KSP-AVC Plugin - 1.1.5
- KW-Rocketry-Community-Fixes - 0.3
- Infernal Robots - 0.21.3
- ModularFlightIntegrator - 1.1.1
- Docking Port Alignment Indicator - 6.2
- Near Future Electrical - 0.5.3
- Procedural Parts - 1.1.7
- RealChute - 1.3.2.3
- RealismOverhaul - 10.4.1
- RealSolarSystem - 10.2
- RemoteTech - 1.6.8
- Saturatable RW - 1.10.2
- SolverEngines - 1.11
- TestFlight - 1.3.1.1
- TestFlight - 1.3.1.1
- TextureReplacer - 2.4.10
- TAC Life Support - 0.11.1.20
- Kerbal Alarm Clock - 3.4
- USI Tools - 0.0.1
- Freight Transport Tech - 0.4.1
- Karbonite - 0.6.4
- Karbonite Plus - 0.5.1
- MKS - 0.31.11.2
- USI-LS - 0.1.5
- USI Survivability Pack - 0.3.2
- Universal Storage - 1.1.0.7
